Name:           kaldi
Version:        5.3
Release:        1%{?dist}
Summary:        A toolkit for speech recognition

License:        ASL 2.0 
URL:            http://kaldi-asr.org/
Source0:        https://github.com/kaldi-asr/%{name}/archive/%{version}.zip 

BuildRequires:  gcc,gcc-c++,make,automake,autoconf,patch,grep,bzip2,gzip,wget,git,svn,gawk,openfst,openfst-devel,perl-Pod-Html,perl-podlators
Requires:       atlas,python2.7,python3,zlib-devel,zlib1g-dev,zlib-devel,libtool,openfst,openfst-devel

%description
Kaldi is a toolkit for speech recognition, intended for use by speech recognition researchers and professionals.

%global debug_package %{nil}

%prep
cd %{_topdir}/BUILD
rm -rf %{name}-%{version}
cd %{_topdir}/SOURCES
unzip 5.3.zip
cd %{_topdir}/BUILD
cp -rf %{_topdir}/SOURCES/%{name}-%{version} .
cp %{_topdir}/SOURCES/%{name}-%{version}/COPYING %{_topdir}/BUILD/
/usr/bin/chmod -Rf a+rX,u+w,g-w,o-w .
rm -rf %{_topdir}/SOURCES/%{name}-%{version}

%build

%{_topdir}/BUILD/%{name}-%{version}/tools/extras/install_sctk_patched.sh

cd %{_topdir}/BUILD/%{name}-%{version}/tools
export CC=gcc && make



cd %{_topdir}/BUILD/%{name}-%{version}/src
./configure

%install
cd %{_topdir}/BUILD/%{name}-%{version}/src
pwd
export CC=gcc && make

%files
%license COPYING



%changelog
* Thu May 10 2018 aacosta@fedoraproject.org - 5.3-1
- Initial version of the package
