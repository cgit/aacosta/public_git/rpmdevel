Name:		howdy	
Version:	1
Release:	1%{?dist}
Summary:	Say Hello ,Texas style

License:	Public Domain
Source0:	howdy
BuildArch:	noarch
%description
A simple program to greet the user, Texas style.

%prep

%build 

%install
mkdir -p %{buildroot}/%{_bindir}
install -p -m 755 %{SOURCE0} %{buildroot}/%{_bindir}

%files
%{_bindir}/howdy

%changelog
* Sat May 06 2017 Alejandro Acosta alxacostaa@gmail.com - 1-1
- Initial Packaging



